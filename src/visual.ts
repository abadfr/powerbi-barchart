"use strict";
// Default Imports

import "core-js/stable";
import "./../style/visual.less";
import powerbi from "powerbi-visuals-api";
import VisualConstructorOptions = powerbi.extensibility.visual.VisualConstructorOptions;
import VisualUpdateOptions = powerbi.extensibility.visual.VisualUpdateOptions;
import IVisual = powerbi.extensibility.visual.IVisual;
import EnumerateVisualObjectInstancesOptions = powerbi.EnumerateVisualObjectInstancesOptions;
import VisualObjectInstance = powerbi.VisualObjectInstance;
import DataView = powerbi.DataView;
import VisualObjectInstanceEnumerationObject = powerbi.VisualObjectInstanceEnumerationObject;

import { VisualSettings } from "./settings";

// Additional Imports
import * as d3 from "d3"; 
type Selection<T extends d3.BaseType> = d3.Selection<T, any,any, any>; // from docs
import IVisualHost = powerbi.extensibility.visual.IVisualHost;
import ISelectionManager = powerbi.extensibility.ISelectionManager;
import ISelectionId = powerbi.visuals.ISelectionId;
import VisualTooltipDataItem = powerbi.extensibility.VisualTooltipDataItem;
import IToolTipService = powerbi.extensibility.ITooltipService;
import { createTooltipServiceWrapper, ITooltipServiceWrapper, TooltipEventArgs } from "powerbi-visuals-utils-tooltiputils";

// interfaces
interface DataPoint {
    category: string;
    value: number;
    color: string;
    selectionID: ISelectionId;
    highlighted: boolean;
    // tooltips: VisualTooltipDataItem[];
}
interface ViewModel { // Separate data and rendering
    dataPoints: DataPoint[];
    maxValue: number;
    highlights: boolean;
}

export class Visual implements IVisual {
    private target: HTMLElement; 
    private settings: VisualSettings;
    private host: IVisualHost;
    private selectionManager: ISelectionManager;
    // containers
    private svg: Selection<SVGElement>;
    private container: Selection<SVGElement>;
    private barGroup: Selection<SVGElement>;

    // bar settings
    private xPadding: number = 0.1;

    // toooltips
    private tooltipServiceWrapper: ITooltipServiceWrapper;

    constructor(options: VisualConstructorOptions) {
        this.host = options.host; // contains settings for the visuals
        this.svg = d3.select(options.element) // parent SVG Element
            .append('svg')
            .classed('my-bar-chart', true);
        // this.svg.style('background-color', 'red');
        this.barGroup = this.svg.append('g') // HTML group element for bars
            .classed('bar-group', true);
        this.selectionManager = this.host.createSelectionManager()
        this.tooltipServiceWrapper = createTooltipServiceWrapper(this.host.tooltipService, options.element);
    }

    public update(options: VisualUpdateOptions) {

        console.log('Updating!');
        let viewModel: ViewModel = this.getViewModel(options);

        let width = options.viewport.width;
        console.log(width);
        let height = options.viewport.height;
        console.log(height);

        this.svg.attr('width', width)
        this.svg.attr('height', height)

        let yScale = d3.scaleLinear()
            .domain([0, viewModel.maxValue])
            .range([height, 0]);
        
        let xScale = d3.scaleBand()
            .domain(viewModel.dataPoints.map(d => d.category))
            .range([0, width])
            .padding(this.xPadding);
        
        let bars = this.barGroup
            .selectAll(".bar") // why dot bar instead of bar?
            .data(viewModel.dataPoints);

        console.log('Setting bar attrs...')
        bars.attr('width', xScale.bandwidth);
        bars.attr('height', d => height - yScale(d.value)); // because of screen coordinates vs cartesian
        bars.attr('y', d => yScale(d.value));
        bars.attr('x', d => xScale(d.category));
        bars.style('fill', d => d.color);
        
        console.log('Entering bars...')
        bars.enter()
            .append("rect")
            .classed("bar", true);
        
        // highlight from other visuals
        bars.style('fill', d => d.color);
        bars.style('fill-opacity', d => viewModel.highlights ? d.highlighted ? 1.0 : 0.5 : 1.0);

        // highlight on click
        bars
            .on('click', (d) => {
                this.selectionManager
                    .select(d.selectionID, true)
                    .then((ids: ISelectionId[]) => {
                        // set all not selected to transparent

                        bars.attr('fill-opacity', ids.length > 0 ? 0.5 : 1.0);
                        const self:this = this;
                        bars.each( (_barDataPoint: DataPoint) => {
                            const isSelected: boolean = self.isSelectionIdInArray(ids, _barDataPoint.selectionID);
                            const opacity: number = isSelected 
                                ? 1.0
                                : 0.5;
                            // d3.select(this).style('fill-opacity', opacity);
                        });
                    });

            });
        
        this.tooltipServiceWrapper.addTooltip(this.barGroup.selectAll('.bar'),
            (tooltipEvent: TooltipEventArgs<DataPoint>) => this.getTooltipData(tooltipEvent.data),
            (tooltipEvent: TooltipEventArgs<DataPoint>) => tooltipEvent.data.selectionID
        )

        bars.exit()
            .remove();
            
    }

    private getViewModel(options: VisualUpdateOptions): ViewModel {
        // get data from Power BI
        let dataView = options.dataViews;
        
        let viewModel: ViewModel = {
            dataPoints: [],
            maxValue: 0,
            highlights: false
        };

        // if no data, return empty viewModel
        if (!dataView
            || !dataView[0]
            || !dataView[0].categorical
            || !dataView[0].categorical.categories
            || !dataView[0].categorical.categories[0].source
            || !dataView[0].categorical.values
            || !dataView[0].metadata)
            return viewModel;
        
        let view = dataView[0].categorical;
        let categories = view.categories[0];
        let values = view.values[0];
        let highlights = values.highlights;
        // for tooltips to get tooltip title
        let metadata = dataView[0].metadata; 
        let categoryColumnName = metadata.columns.filter(c => c.roles['category'])[0].displayName;
        let valueColumnName = metadata.columns.filter(c => c.roles['measure'])[0].displayName;

        // set dataPoints
        for (let i = 0, len = Math.max(categories.values.length, values.values.length); i < len; i++) {
            console.log('pushing data to viewModel...')
            viewModel.dataPoints.push({
                category: <string>categories.values[i],
                value: <number>values.values[i],
                color: this.host.colorPalette.getColor(<string>categories.values[i]).value,
                selectionID: this.host.createSelectionIdBuilder()
                    .withCategory(categories, i)
                    .createSelectionId(),
                highlighted: highlights ? highlights[i] ? true : false : false,
                // tooltips: [{
                //     displayName: categoryColumnName,
                //     value: <string>categories.values[i]
                // }, {
                //     displayName: categoryColumnName,
                //     value: (<number>values.values[i]).toFixed(2)
                // }]
            }) // How about two categories?
        }
        // set maxValue
        viewModel.maxValue = d3.max(viewModel.dataPoints, d => d.value);

        // set highlights
        viewModel.highlights = viewModel.dataPoints.filter(d => d.highlighted).length > 0;

        return viewModel;
    }

    private isSelectionIdInArray(selectionIds: ISelectionId[], selectionId: ISelectionId): boolean {
        if (!selectionIds || !selectionId) {
            return false;
        }

        return selectionIds.some((currentSelectionId: ISelectionId) => {
            return currentSelectionId.includes(selectionId);
        });
    }

    private getTooltipData(value: any): VisualTooltipDataItem[] {
        return [{
            displayName: value.category,
            value: value.value.toString(),
            color: value.color,
        }];
    }

    // private static parseSettings(dataView: DataView): VisualSettings {
    //     return <VisualSettings>VisualSettings.parse(dataView);
    // }

    // /**
    //  * This function gets called for each of the objects defined in the capabilities files and allows you to select which of the
    //  * objects and properties you want to expose to the users in the property pane.
    //  *
    //  */
    // public enumerateObjectInstances(options: EnumerateVisualObjectInstancesOptions): VisualObjectInstance[] | VisualObjectInstanceEnumerationObject {
    //     return VisualSettings.enumerateObjectInstances(this.settings || VisualSettings.getDefault(), options);
    // }
}